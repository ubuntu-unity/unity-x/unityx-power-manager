			      UnityX Power Manager
			   A Power Manager for MATE

UnityX Power Manager is a UnityX session daemon that acts as a policy agent on
top of UPower. It listens to system events and responds with user-configurable
actions.

UnityX Power Manager comes in three main parts:

	- unityx-power-manager:	     the manager daemon itself
	- unityx-power-preferences:  the control panel program, for configuration
	- unityx-power-statistics:   the statistics graphing program

To build, UnityX Power Manager requires

	- Glib (2.36.0 or later)
	- GTK+3 (3.14.0 or later)
	- libsecret (0.11 or later)
	- GNOME Keyring (3.0.0 or later)
	- DBus (0.70 or later)
	- libnotify (0.7.0 or later)
	- Cairo (1.0.0 or later)
	- libmate-panel-applet (1.17.0 or later)
	- xrandr (1.3.0 or later)
	- Canberra (0.10 or later)
	- UPower (0.9.5 or later)

To work properly, unityx-power-manager requires udevd and upowerd to be running.

UnityX Power Manager is a fork of GNOME Power Manager.

For more information, please see https://unityd.org/
